﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using umbraco.BusinessLogic.Actions;
using Umbraco.Core;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace Area51.Web.App_Code
{
    [PluginController("Area51")]
    [Umbraco.Web.Trees.Tree("Area51", "Area51Tree", "Dashboard")]
    public class Area51TreeController : TreeController {
        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings) {
            var nodes = new TreeNodeCollection();
            //if (id == Constants.System.Root.ToInvariantString()) {
            //    var item = this.CreateTreeNode("lobby", null, queryStrings, "Lobby", "icon-rocket", true);
            //    nodes.Add(item);
            //}
            if (id == Constants.System.Root.ToInvariantString()) {
                var item = this.CreateTreeNode("signalrStatusChecker", id, queryStrings, "Process status checker", "icon-bulleted-list", false, "Area51/Area51Tree/signalrStatusChecker/" + id.ToString());
                nodes.Add(item);
            }
            return nodes;
        }
        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings) {
            return null;
        }
    }
}
