﻿using Area51.Web.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Area51.Web.Controllers
{
    public class StatusCheckerController : ApiControllerWithHub<ProcessStatusCheckerHub> {
        public void NewTask(string s) {
            // Notify the connected clients
            Hub.Clients.All.broadcastMessage(s);
        }
    }
}