﻿using Microsoft.Owin;
using Owin;
using Umbraco.Web;

[assembly: OwinStartup("Startup",typeof(Area51.Web.Startup))]
namespace Area51.Web {
    public class Startup : UmbracoDefaultOwinStartup {
        public override void Configuration(IAppBuilder app) {
            // Any connection or hub wire up and configuration should go here
            base.Configuration(app);
            app.MapSignalR();
        }
    }
}