﻿using Area51.Web.Controllers;
using Area51.Web.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.WebApi;

namespace Area51.Web.Area51_Controllers.Web.Helpers.Plugin
{
    public class ProcessStatusCheckerApiController : UmbracoApiController {
        [HttpPost]
        public string DummyProcess() {
            StatusCheckerController task = new StatusCheckerController();
            string s = "Long process is starting . . .";
            task.NewTask(s);

            // Task A
            Thread.Sleep(1000);
            s = "Task A done";
            task.NewTask(s);

            // Task B
            Thread.Sleep(3000);
            s = "Task B done";
            task.NewTask(s);

            // Task C
            Thread.Sleep(1500);
            s = "Task C done";
            task.NewTask(s);

            Thread.Sleep(500);
            s = "Process successfully completed!";
            task.NewTask(s);

            return " ";
        }
    }
}