﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.WebApi;
using System.Web.Http;
using Umbraco.Core.Models;
using umbraco.cms.businesslogic.web;
using Umbraco.Core;
using umbraco;
using System.Net;

namespace Area51.Web.Controllers.Api {
    public class Area51ApiController : UmbracoAuthorizedApiController {
        [HttpPost]
        public string DummyProcess() {
            string s = "Hello from DummyProcess()";
            return s;
        }
    }
}